/* ********** bcryptjs ********** */

var bcryptjs = require('bcryptjs');

exports.getHash = (password, saltRounds) => {
  return bcryptjs.hashSync(password, saltRounds);
};

exports.comparePasswords = (passwordA, passwordB) => {
  return bcryptjs.compareSync(passwordA, passwordB);
};

/* ********** jsonwebtoken ********** */

var jsonwebtoken = require('jsonwebtoken');
var config = require('../config/config');

var secretKey = config.secretKey;
var sessionExpirationTime = config.sessionExpirationTime;

exports.globalExpirationTime = sessionExpirationTime;

exports.getJWT = (idcliente, expiresIn) => {
  var payload = {
    algorithm: 'HS256',
  };
  if (expiresIn) payload.expiresIn = expiresIn;
  return jsonwebtoken.sign({ idcliente }, secretKey, payload);
};

exports.verifyJWT = (accesToken) => {
  return jsonwebtoken.verify(accesToken, secretKey);
};

/* ********** random ********** */

exports.getRandomID = () => {
  var fecha = new Date().getTime();
  var random = Math.floor(Math.random() * 1000);
  return `${fecha}${random}`;
};
