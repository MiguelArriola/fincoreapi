var locatinService = require('../_services/locatinService');

exports.getLocations = async () => {
  var data;
  var client = locatinService.getClient();
  var success = false;
  var intentos = 0;
  while (!success) {
    try {
      data = await client.get('');
      success = true;
    } catch (error) {}
    intentos += 1;
    if (intentos > 5) break;
  }
  if (!success) return 500;
  var results = data.body.results;
  var locations = results.map(
    ({ calle, longitude, latitude, codigopostal }, index) => {
      var type = index % 2 === 0 ? 'SUCURSAL' : 'ATM';
      return {
        street: calle,
        longitude,
        latitude,
        type,
        zip: codigopostal,
      };
    }
  );
  return locations;
};
