var db = require('../_db/_db');

exports.get = async (idcliente) => {
  var collection = `Clientes`;
  var query = `q={"idcliente":"${idcliente}"}`;
  var filter = `&f={"cuentas":1}`;
  var client = db.getClient(collection, query, filter);
  var result = await client.get('');
  return result;
};

exports.verifyAccount = async (account) => {
  var collection = `Clientes`;
  var query = `q={'cuentas': { $elemMatch: { 'idcuenta': '${account}'} } }`;
  var count = `c=true`;
  var client = db.getClient(collection, query, count);
  var result = await client.get('');
  return result;
};

exports.getBalance = async (account) => {
  var collection = `Clientes`;
  var query = `q={'cuentas': { $elemMatch: { 'idcuenta': '${account}'} } }`;
  var filter = `f={'cuentas.saldo':1}`;
  var client = db.getClient(collection, query, filter);
  var result = await client.get('');
  return result;
};

exports.updateBalance = async (account, balance) => {
  var collection = `Clientes`;
  var query = `q={'cuentas': { $elemMatch: { 'idcuenta': '${account}'} } }`;
  var filter = `f={'cuentas.saldo':1}`;
  var client = db.getClient(collection, query, filter);
  var data = { $set: { 'cuentas.$.saldo': balance } };
  var result = await client.put('', data);
  return result;
};
