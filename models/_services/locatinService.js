var requestjson = require('request-json');
var config = require('../../config/config');

var api = config.locationServiceAPI;

exports.getClient = () => {
  var client = requestjson.createClient(api);
  return client;
};
