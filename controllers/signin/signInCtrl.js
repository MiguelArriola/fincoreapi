var helpers = require('../../helpers/helpers');
var customersModel = require('../../models/customers/customersModel');

exports.post = async (request, response) => {
  var email = request.body.email;
  try {
    var result = await customersModel.getCredentials(email);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var credentials = result.body;
  if (credentials < 1) {
    var message = `Email o contraseña incorrectos`;
    return response.status(401).send({ message }).end(); // email no existe
  }
  var password = request.body.password;
  var criptedPasswor = credentials[0].password;
  var passwordVerification = helpers.comparePasswords(password, criptedPasswor);
  if (passwordVerification === false) {
    var message = `Email o contraseña incorrectos`;
    return response.status(401).send({ message }).end(); // contraseña invalida
  }
  var idcliente = credentials[0].idcliente;
  var expiresIn = helpers.globalExpirationTime;
  var accessToken = helpers.getJWT(idcliente, expiresIn);
  return response.status(200).json({ accessToken, expiresIn }).end();
};
