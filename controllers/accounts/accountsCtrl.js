var accountsModel = require('../../models/accounts/accountsModel');

exports.get = async (request, response) => {
  idcliente = request.idcliente;
  try {
    var result = await accountsModel.get(idcliente);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var accounts = result.body[0].cuentas;
  return response.status(200).send(accounts).end();
};
