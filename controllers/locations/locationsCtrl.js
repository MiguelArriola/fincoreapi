var locationsModel = require('../../models/locations/locationsModel');

exports.get = async (request, response) => {
  var result = await locationsModel.getLocations();
  if (result === 500) return response.status(500).send().end(); // error del servidor
  return response.status(200).send(result).end();
};
